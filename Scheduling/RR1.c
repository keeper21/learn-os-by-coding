#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QUANTUM 4
#define MAXSIZE 20
/*this is a round robin process without priority,
so there should be a queue for ready processes to
implement "first in first out", and be careful with 
the process number, a larger number of process requires
a larger number of data size(MAXSIZE) in queue.
*/
struct PCB{
	char id[5];
	int arrival;
	int burst;
	////we dont need priority in normal RR Scheduling
	int priority;
	int waiting;
	int turnaround;
	int remain;
	//0 for not in queue and 1 for in queue
	int inQueue;
};

typedef struct{
	int data[MAXSIZE];
	int front;
	int rear;
	int size;
}Queue;

Queue* createQueue();
void readFile(char* fileName);
int cmp(const void* a, const void* b);
void rr();
int isFull();
int isEmpty();
void enQueue(int i);
int deQueue();
float att();
float awt();

//the array to store PCB
static struct PCB arr[5];
static Queue* queue;

//the process Information is stored in PCB.txt
int main(int argc, char *argv[]){
	readFile(argv[1]);
	queue = createQueue();
	//first sort in arrival order
	qsort(arr, 5, sizeof(struct PCB), cmp);
	//then sort in RR
	rr();
	
	printf("\n\nRound Robin without priority\n");
	printf("Process Arrival Burst   Waiting Turnaround\n");
	for(int i = 0; i < 5; i++)
		printf("%s\t%d\t%d\t%d\t%d\n", arr[i].id, arr[i].arrival, arr[i].burst, arr[i].waiting, arr[i].turnaround);
	printf("Average turnaround time: %.2f\n", att());	
	printf("Average waiting time: %.2f\n", awt());	
	free(queue);
	return 0;
}

Queue* createQueue(){
	Queue* q = (Queue*)malloc(sizeof(Queue));
	
	q->front = -1;
	q->rear = -1;
	q->size = 0;
	return q;
}

void readFile(char* fileName){
	FILE* fp;
	char fileLine[20];
	char* splitted;
	
	fp = fopen(fileName, "r");
	if(fp == NULL){
		printf("Failed to open \"%s\", Please check name and path!\n", fileName);
		exit(0);
	}
	
	for(int i = 0; i < 5 && fgets(fileLine, 19, fp) != NULL; i++) {
		/*in txt file, if press ENTER, it actually store '\r' then '\n' not only '\n' */
		splitted = strtok(fileLine, ",\r");
		strcpy(arr[i].id, splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].arrival = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].burst = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].priority = atoi(splitted);
		arr[i].remain = arr[i].burst;
		arr[i].inQueue = 0;
	}
}

int cmp(const void* a, const void* b){
    if( ((struct PCB*)a)->arrival < ((struct PCB*)b)->arrival) 
    	return -1;
    return 1;
}

void rr(){
	//ct->current time, counter is used for trigger
	int ct = 0, i = 0, counter = 0;
	//rest->number of not finished process
	int rest = 5; 
	while(rest != 0){
		if(arr[i].remain <= QUANTUM && arr[i].remain > 0){
			ct += arr[i].remain;
			arr[i].remain = 0;
			counter = 1;
			printf("\nrun%d ", i+1);
		}
		else if(arr[i].remain > 0){
			ct += QUANTUM;
			arr[i].remain -= QUANTUM;
			printf("\nrun%d ", i+1);
		}
		
		//arr[i] finished
		if(arr[i].remain == 0 && counter == 1){
			rest--;
			arr[i].turnaround = ct - arr[i].arrival;
			arr[i].waiting = arr[i].turnaround - arr[i].burst;
			counter = 0;
		}
		//arr[i] not finished, run next process with new quantum
		else{
			//store i value before using i++ to insert next possible process in queue
			int temp = i;
			if(arr[i+1].arrival <= ct){
				while(i < 4 && arr[i+1].arrival <= ct && (arr[i+1].remain > 0)){
					if(!arr[i+1].inQueue){
						enQueue(i+1);
						arr[i+1].inQueue = 1;
						printf("enqueue%d ", i+2);
					}
					i++;
				}
			}
			//put current not finished process in queue
			if(arr[temp].remain > 0){
				if(!arr[temp].inQueue){
					enQueue(temp);
					arr[temp].inQueue = 1;
					printf("enqueueRemain%d ", temp+1);
				}
			}
			//number of ready process is not zero
			if(!isEmpty()){
				i = deQueue();
				arr[i].inQueue = 0;
				printf("dequeue%d ", i+1);
			}
		}
	}
}

int isFull(){
	return (queue->size == MAXSIZE);
}

int isEmpty(){
	return (queue->size == 0);
}

void enQueue(int i){
	if(isFull()){
		printf("queue is Full!\n");
		return;
	}
	queue->rear++;
	queue->rear %= MAXSIZE;
	queue->size++;
	queue->data[queue->rear] = i;
}

int deQueue(){
	if(isEmpty()){
		printf("queue is Empty!\n");
		return 0;
	}
	queue->front++;
	queue->front %= MAXSIZE;
	queue->size--;
	return (queue->data[queue->front]);
}

//att->average turnaround time
float att(){
	float sum = 0.0;
	for(int i = 0; i < 5; i++)
		sum += arr[i].turnaround;
	return sum/5;
}

//awt->average waiting time
float awt(){
	float sum = 0.0;
		for(int i = 0; i < 5; i++)
			sum += arr[i].waiting;
	return sum/5;
}
