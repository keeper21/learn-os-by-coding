#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QUANTUM 4
#define MAXSIZE 20
/*this is a nonpreemptive round robin process with priority,
so we dont need the queue now, because the next process in 
ready processes is always the one with highest priority.
*/
struct PCB{
	char id[5];
	int arrival;
	int burst;
	//0 for low 1 for middle and 2 for high
	int priority;
	int waiting;
	int turnaround;
	int remain;
	//0 for not in ready status and 1 for in
	int inReady;
	int justRun;
};

void readFile(char* fileName);
int cmp(const void* a, const void* b);
void rr();
void addInReady(int i);
int outFromReady();
int isEmpty();
char* getPriority(int i);
float att();
float awt();

//the array to store PCB
static struct PCB arr[5];
static char priorityStr[10];
static int readyArr[5] = {-1, -1, -1, -1, -1};

//the process Information is stored in PCB.txt
int main(int argc, char *argv[]){
	readFile(argv[1]);
	//first sort in arrival order
	qsort(arr, 5, sizeof(struct PCB), cmp);
	//then sort in RR
	rr();
	
	printf("\n\nRound Robin with priority\n");
	printf("Process Arrival Burst   Waiting Turnaround Priority\n");
	for(int i = 0; i < 5; i++)
		printf("%s\t%d\t%d\t%d\t%d\t   %s\n", arr[i].id, arr[i].arrival, arr[i].burst, arr[i].waiting, arr[i].turnaround, getPriority(i));
	printf("Average turnaround time: %.2f\n", att());	
	printf("Average waiting time: %.2f\n", awt());	
	return 0;
}

void readFile(char* fileName){
	FILE* fp;
	char fileLine[20];
	char* splitted;
	
	fp = fopen(fileName, "r");
	if(fp == NULL){
		printf("Failed to open \"%s\", Please check name and path!\n", fileName);
		exit(0);
	}
	
	for(int i = 0; i < 5 && fgets(fileLine, 19, fp) != NULL; i++) {
		/*in txt file, if press ENTER, it actually store '\r' then '\n' not only '\n' */
		splitted = strtok(fileLine, ",\r");
		strcpy(arr[i].id, splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].arrival = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].burst = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].priority = atoi(splitted);
		arr[i].remain = arr[i].burst;
		arr[i].inReady = 0;
	}
}

int cmp(const void* a, const void* b){
    if( ((struct PCB*)a)->arrival < ((struct PCB*)b)->arrival) 
    	return -1;
    return 1;
}

void rr(){
	//ct->current time, counter is used for trigger
	int ct = 0, i = 0, counter = 0;
	//rest->number of not finished process
	int rest = 5; 
	while(rest != 0){
		if(arr[i].remain <= QUANTUM && arr[i].remain > 0){
			ct += arr[i].remain;
			arr[i].remain = 0;
			counter = 1;
			printf("\nrunP%d ", i+1);
		}
		else if(arr[i].remain > 0){
			ct += QUANTUM;
			arr[i].remain -= QUANTUM;
			printf("\nrunP%d ", i+1);
		}
		
		//arr[i] finished
		if(arr[i].remain == 0 && counter == 1){
			rest--;
			arr[i].turnaround = ct - arr[i].arrival;
			arr[i].waiting = arr[i].turnaround - arr[i].burst;
			counter = 0;
		}
		//arr[i] not finished, run next process with new quantum
		else{
			//store i value before using i++ to find next process
			int temp = i;
			if(arr[i+1].arrival <= ct){
				while(i < 4 && arr[i+1].arrival <= ct && (arr[i+1].remain > 0)){
					if(!arr[i+1].inReady){
						addInReady(i+1);
						arr[i+1].inReady = 1;
						printf("inReadyP%d ", i+2);
					}
					i++;
				}
			}
			if(arr[temp].remain > 0){
				if(!arr[temp].inReady){
					addInReady(temp);
					arr[temp].inReady = 1;
					arr[temp].justRun = 1;
					printf("inReadyRemainP%d ", temp+1);
				}
			}
			if(!isEmpty()){
				i = outFromReady();
				arr[i].inReady = 0;
				//update flag "justRun"
				if(arr[temp].justRun == 1){
					arr[temp].justRun = 0;
					printf("P%djustrun ", temp+1);
				}
				printf("outReadyP%d ", i+1);
			}
		}
	}
}

//add the index of process in ready array
void addInReady(int i){
	for(int j = 0; j < 5; j++){
		if(readyArr[j] == -1){
			readyArr[j] = i;
			//printf("INready[%d]=%d\n ", j, readyArr[j]);
			return;
		}
		
	}
	printf("ready array is full!");
	return;
}

/*return the index of process with highest
priority among all ready processes, make sure
the running process in last quantum is not the 
same in current quantum(by checking flag "justRun")*/
int outFromReady(){
	int max = -1;
	int maxIndex = 0;
	for(int i = 0; i < 5; i++){
		if(arr[readyArr[i]].priority > max && !arr[readyArr[i]].justRun){
			max = arr[readyArr[i]].priority;
			maxIndex = i;
		}
		//printf("OUTready[%d]=%d\n ", i, readyArr[i]);
	}
	int temp = readyArr[maxIndex];
	readyArr[maxIndex] = -1; 
	return temp;
}

//0 for not empty and 1 for empty(no ready process)
int isEmpty(){
	for(int i = 0; i < 5; i++)
		if(readyArr[i] != -1)
			return 0;
	return 1;
}

char* getPriority(int i){
	if(arr[i].priority == 0)
		strcpy(priorityStr, "low");
	else if(arr[i].priority == 1)
		strcpy(priorityStr, "middle");
	else
		strcpy(priorityStr, "high");
	return priorityStr;
}

//att->average turnaround time
float att(){
	float sum = 0.0;
	for(int i = 0; i < 5; i++)
		sum += arr[i].turnaround;
	return sum/5;
}

//awt->average waiting time
float awt(){
	float sum = 0.0;
		for(int i = 0; i < 5; i++)
			sum += arr[i].waiting;
	return sum/5;
}
