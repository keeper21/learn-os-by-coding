#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXNUM 5

void readFile(char* fileName);
int cmp(const void* a, const void* b);
float att();
float awt();

struct PCB{
	char id[5];
	int arrival;
	int burst;
	//we dont need priority in FCFS Scheduling
	int priority;
	int waiting;
	int turnaround;
};
//the array to store PCB
static struct PCB arr[MAXNUM];

//the process Information is stored in PCB.txt
int main(int argc, char *argv[]){
	readFile(argv[1]);
	
	qsort(arr, MAXNUM, sizeof(struct PCB), cmp);
	
	float averageTurnaround = att();
	float averageWaiting = awt();
	
	printf("First Come First Serve\n");
	printf("Process Arrival Burst   Waiting Turnaround\n");
	for(int i = 0; i < MAXNUM; i++)
		printf("%s\t%d\t%d\t%d\t%d\n", arr[i].id, arr[i].arrival, arr[i].burst, arr[i].waiting, arr[i].turnaround);
	printf("Average turnaround time: %.2f\n", averageTurnaround);	
	printf("Average waiting time: %.2f\n", averageWaiting);	
	return 0;
}

void readFile(char* fileName){
	FILE* fp;
	char fileLine[20];
	char* splitted;
	
	fp = fopen(fileName, "r");
	if(fp == NULL){
		printf("Failed to open \"%s\", Please check name and path!\n", fileName);
		exit(0);
	}
	
	for(int i = 0; i < MAXNUM && fgets(fileLine, 19, fp) != NULL; i++) {
		//in txt file, if press ENTER, it actually store '\r' then '\n' not only '\n'
		splitted = strtok(fileLine, ",\r");
		strcpy(arr[i].id, splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].arrival = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].burst = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].priority = atoi(splitted);
	}
}	

//define cmp so we can use quicksort()
int cmp(const void* a, const void* b){
    if( ((struct PCB*)a)->arrival < ((struct PCB*)b)->arrival) 
    	return -1;
    return 1;
}

//att->average turnaround time
float att(){
	float sum = 0.0;
	for(int i = 0; i < MAXNUM; i++){
		for(int j = 0; j < i + 1; j++)
			arr[i].turnaround += arr[j].burst;
		arr[i].turnaround -= arr[i].arrival;
		sum += arr[i].turnaround;
	}
	return sum/MAXNUM;
}

//awt->average waiting time
float awt(){
	float sum = 0.0;
	for(int i = 0; i < MAXNUM; i++){
		arr[i].waiting = arr[i].turnaround - arr[i].burst;
		sum += arr[i].waiting; 
	}
	return sum/MAXNUM;
}
