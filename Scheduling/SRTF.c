#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXNUM 5
#define executed 1
#define not_exec 0


void readFile(char* fileName);
int cmp(const void* a, const void* b);
void srtf();
int shortestProcess(int ct);
float att();
float awt();

struct PCB{
	char id[5];
	int arrival;
	int burst;
	//we dont need priority in SRTF Scheduling
	int priority;
	int waiting;
	int turnaround;
	int remain;
	int status;
};
//the array to store PCB
static struct PCB arr[MAXNUM];

//the process Information is stored in PCB.txt
int main(int argc, char *argv[]){
	readFile(argv[1]);
	//first sort in arrival order
	qsort(arr, MAXNUM, sizeof(struct PCB), cmp);
	//then sort in SRTF order
	srtf();
	
	printf("Shortest Remaining Time First\n");
	printf("Process Arrival Burst   Waiting Turnaround\n");
	for(int i = 0; i < MAXNUM; i++)
		printf("%s\t%d\t%d\t%d\t%d\n", arr[i].id, arr[i].arrival, arr[i].burst, arr[i].waiting, arr[i].turnaround);
	printf("Average turnaround time: %.2f\n", att());	
	printf("Average waiting time: %.2f\n", awt());	
	return 0;
}

void readFile(char* fileName){
	FILE* fp;
	char fileLine[20];
	char* splitted;
	
	fp = fopen(fileName, "r");
	if(fp == NULL){
		printf("Failed to open \"%s\", Please check name and path!\n", fileName);
		exit(0);
	}
	
	for(int i = 0; i < MAXNUM && fgets(fileLine, 19, fp) != NULL; i++) {
		//in txt file, if press ENTER, it actually store '\r' then '\n' not only '\n'
		splitted = strtok(fileLine, ",\r");
		strcpy(arr[i].id, splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].arrival = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].burst = atoi(splitted);
		splitted = strtok(NULL, ",\r");
		arr[i].priority = atoi(splitted);
		arr[i].remain = arr[i].burst;
	}
}

int cmp(const void* a, const void* b){
    if( ((struct PCB*)a)->arrival < ((struct PCB*)b)->arrival) 
    	return -1;
    return 1;
}

//processes must in arrival order before this
void srtf(){
	//ct->current time, spi->shortest process index
	int i, ct = 0, spi; 
	//initialising each process as not executed
	for(int i = 0; i < MAXNUM; i++){
		arr[i].status = not_exec;
		arr[i].waiting = 0;
	}
	do{
		spi = shortestProcess(ct);
		//no ready process
		if(spi == -1)
			break;

		//increase the waiting time of processes which have arrived but are not executing(in ready list)
		for(i = 0; i < MAXNUM; i++){
			if(i != spi && arr[i].status != executed && ct >= arr[i].arrival)
				arr[i].waiting++;
		}
		ct++;
		arr[spi].turnaround = ct - arr[spi].arrival;
		arr[spi].remain--;
		if(arr[spi].remain <= 0){
			arr[spi].status= executed;
			arr[spi].turnaround = ct - arr[spi].arrival;
		}
	} while(ct);
}

//get Index of the shortest process at current time
int shortestProcess(int ct){
	int i, min_burst = 99999, min_index = -1;
	for(i = 0; i < MAXNUM; i++){
		/*arr[i] is in ready list*/
		if(arr[i].status == not_exec && arr[i].arrival <= ct){
			if(arr[i].remain < min_burst){
			    min_burst = arr[i].remain;
				min_index = i;
			}
		}
	}
	return min_index;
}

//att->average turnaround time
float att(){
	float sum = 0.0;
	for(int i = 0; i < MAXNUM; i++)
		sum += arr[i].turnaround;
	return sum/MAXNUM;
}

//awt->average waiting time
float awt(){
	float sum = 0.0;
		for(int i = 0; i < MAXNUM; i++)
			sum += arr[i].waiting;
	return sum/MAXNUM;
}

/* refer to following
https://github.com/curiousguy13/os-programs/blob/master/srtf.c
*/
